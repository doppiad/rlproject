// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
namespace RLDataLayer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AgentValues
    {
        public int ID { get; set; }

        public int ID_Alias { get; set; }

        [Required]
        public double vGoalX { get; set; }

        [Required]
        public double vGoalY { get; set; }

        [Required]
        public double vGoalZ { get; set; }

        public double dGoal { get; set; }

        public DateTime timestamp { get; set; }

        public virtual Agent Agent { get; set; }
        public int Epoch { get; set; }
        public int Episode { get; set; }
        public int xPosition { get; set; }
        public int zPosition { get; set; }
        public int lastDecision { get; set; }
        public double speed { get; set; }
    }
}