// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
namespace RLDataLayer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Agent")]
    public partial class Agent
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Agent()
        {
            AgentRaysValues = new HashSet<AgentRaysValues>();
            AgentValues = new HashSet<AgentValues>();
        }

        public int ID { get; set; }

        public int ID_Session { get; set; }

        [Required]
        [StringLength(255)]
        public string Alias { get; set; }

        public virtual Session Session { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AgentRaysValues> AgentRaysValues { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AgentValues> AgentValues { get; set; }
    }
}