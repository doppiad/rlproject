namespace RLDataLayer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AgentRaysValues
    {
        public int ID { get; set; }

        public int ID_Ray { get; set; }

        public int ID_Alias { get; set; }

        public double dHit { get; set; }

        public int hitCount { get; set; }

        public double vHit { get; set; }

        public virtual Agent Agent { get; set; }
    }
}
