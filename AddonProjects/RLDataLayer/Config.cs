// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca

using System.Linq;

namespace RLDataLayer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Config")]
    public partial class Config
    {
        public int ID { get; set; }

        public double yShift { get; set; }

        public int RaysNumber { get; set; }

        public double maxDistance { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public double WallRotationAngle { get; set; }
        public double maxWalkingSpeed { get; set; }
        public double minWalkingSpeed { get; set; }
        public int decisionPerMinute { get; set; }

        public static List<String> VariablesList()
        {
            return typeof(Config)
                .GetFields()
                .Select(field => field.Name)
                .ToList();
        }
    }
}