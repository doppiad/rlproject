// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
namespace RLDataLayer
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class RLDatabase : DbContext

    {
        public RLDatabase()
            : base("data source=localhost;initial catalog=RL;User ID=sa;Password=RLPassword;App=EntityFramework;")
        {
        }

        public virtual DbSet<Agent> Agent { get; set; }
        public virtual DbSet<AgentRaysValues> AgentRaysValues { get; set; }
        public virtual DbSet<AgentValues> AgentValues { get; set; }
        public virtual DbSet<Session> Session { get; set; }
        public virtual DbSet<Config> Config { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Agent>()
                .HasMany(e => e.AgentRaysValues)
                .WithRequired(e => e.Agent)
                .HasForeignKey(e => e.ID_Alias)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Agent>()
                .HasMany(e => e.AgentValues)
                .WithRequired(e => e.Agent)
                .HasForeignKey(e => e.ID_Alias)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Session>()
                .HasMany(e => e.Agent)
                .WithRequired(e => e.Session)
                .HasForeignKey(e => e.ID_Session)
                .WillCascadeOnDelete(false);
        }
    }
}