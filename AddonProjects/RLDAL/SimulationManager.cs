﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RLDataLayer;

namespace RLDAL
{
    /// <summary>
    /// This class has been design to hold all methods to manage the communication
    /// between unity simulator and DB, in particular this class has the logic for Simulation
    /// </summary>
    public class SimulationManager
    {
        /// <summary>
        /// Generate on DB new session to link with agents
        /// </summary>
        /// <param name="s">Session</param>
        /// <returns>DB-generated simulation ID</returns>
        public static int CreateNewSimulation(Session s)
        {
            using (var db = new RLDatabase())
            {
                var toReturn = db.Session.Add(s);
                db.SaveChanges();
                return toReturn.ID;
            }
        }

        //TODO further development
    }
}