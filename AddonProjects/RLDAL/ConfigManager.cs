﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using RLDataLayer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLDAL
{
    /// <summary>
    /// This class has been design to hold all methods to manage the communication
    /// between unity simulator and DB, in particular this class has the logic for Config
    /// Config hold all the parameters for a single simulation
    /// </summary>
    public class ConfigManager
    {
        /// <summary>
        /// Setted from main menu. Test for debug
        /// </summary>
        public static string _configurationSelected = "test";

        /// <summary>
        /// Every single configuration is identified by a numerical ID unknown to final user
        /// and a name. It can be chosen from main menu
        /// </summary>
        /// <param name="Name">Configuration name</param>
        /// <returns></returns>
        public static Config GetConfigurationByName(string Name = "test")
        {
            using (var db = new RLDatabase())
            {
                var configToReturn = db.Config.FirstOrDefault(x => x.Name == Name);
                return configToReturn;
            }
        }

        /// <summary>
        /// This method returns all the possible configuration saved on DB
        /// Useful for shows all the configuration on main menu
        /// </summary>
        /// <returns></returns>
        public static List<String> getConfigurationNames()
        {
            using (var db = new RLDatabase())
            {
                var configToReturn = db.Config.Select(x => x.Name).ToList();
                return configToReturn;
            }
        }

        /// <summary>
        /// To avoid rewriting main menu and method to edit every single field from main menu
        /// this method uses Reflection to manage every single field.
        /// </summary>
        /// <param name="configName">Configuration name</param>
        /// <param name="fieldName">Field edited</param>
        /// <param name="val">New Value</param>
        /// <returns>0 for save success, error otherwise (ref. EF6) </returns>
        public static int setSingleValue(string configName, string fieldName, object val)
        {
            using (var db = new RLDatabase())
            {
                var c = db.Config.FirstOrDefault(x => x.Name == configName);
                c.GetType().GetProperty(fieldName).SetValue(c, Convert.ChangeType(val, c.GetType().GetProperty(fieldName).PropertyType));
                db.Entry(c).State = EntityState.Modified;
                return db.SaveChanges();
            }
        }

        /// <summary>
        /// This method return a tuple with the number of RL decision per minute and the equivalent as FPS
        /// </summary>
        /// <returns>Tuple with decision per minute, FPS</returns>
        public static Tuple<int, float> getDecisionFps()
        {
            using (var db = new RLDatabase())
            {
                var decision = db.Config.FirstOrDefault(x => x.Name == _configurationSelected).decisionPerMinute;
                return new Tuple<int, float>(decision, (float)(1f / decision));
            }
        }

        /// <summary>
        /// For selected config returns min and max speed value
        /// </summary>
        /// <returns>Tuple with min and max speed (in m/s)</returns>
        public static Tuple<double, double> getSpeedLimit()
        {
            using (var db = new RLDatabase())
            {
                var config = db.Config.FirstOrDefault(x => x.Name == _configurationSelected);
                return new Tuple<double, double>(config.minWalkingSpeed, config.maxWalkingSpeed);
            }
        }
    }
}