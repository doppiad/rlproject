﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RLDataLayer;

namespace RLDAL
{
    /// <summary>
    /// This class has been design to hold all methods to manage the communication
    /// between unity simulator and DB, in particular this class has the logic for Agent Values
    /// </summary>
    public static class AgentValuesManager
    {
        /// <summary>
        /// DB object. It will be probably removed in favor of Using statement
        /// </summary>
        private static RLDatabase _db = new RLDatabase();

        /// <summary>
        /// Number of entities need to be saved
        /// </summary>
        private static int _toSave = 0;

        /// <summary>
        /// Session ID for further development. Foreign key for agents
        /// </summary>
        private static int _sessionId = -1;

        /// <summary>
        /// Flag to avoid new session id generation
        /// </summary>
        private static bool _setSession = false;

        /// <summary>
        /// Setter for session ID. This class doesn't generate SessionID so it should be stored in variable
        /// </summary>
        /// <param name="id">Sessoin ID</param>
        public static void setSessionId(int id)
        {
            if (!_setSession)
            {
                _setSession = true;
                _sessionId = id;
            }
        }

        /// <summary>
        /// All the agent inside the simulator have a name, they should be registered for
        /// further analysis
        /// </summary>
        /// <param name="a">Agent type to be saved</param>
        /// <returns></returns>
        public static int registerAgent(Agent a)
        {
            a.ID_Session = _sessionId;
            var toReturn = _db.Agent.Add(a);
            _db.SaveChanges();
            return toReturn.ID;
        }

        /// <summary>
        /// Every agents inside the simulation generate some values
        /// they should be saved on DB in order to do RL
        /// </summary>
        /// <param name="av">Values for single agent</param>
        /// <returns></returns>
        public static string appendValuesToContext(AgentValues av)
        {
            _db.AgentValues.Add(av);
            _toSave++;
            if (_toSave % 100 == 0) //Actually saves on DB after queue contains at least 100 items. It should be moved as a parameter and configured from DB
            {
                try
                {
                    _db.SaveChanges();
                }
                catch (Exception e)
                {
                    return e.InnerException.Message; //TODO use log4net
                }
                return "SAVE OK!";
            }

            return _toSave.ToString();
        }
    }
}