// Il blocco ifdef seguente viene in genere usato per creare macro che semplificano
// l'esportazione da una DLL. Tutti i file all'interno della DLL sono compilati con il simbolo RLDATABASE_EXPORTS
// definito nella riga di comando. Questo simbolo non deve essere definito in alcun progetto
// che usa questa DLL. In questo modo qualsiasi altro progetto i cui file di origine includono questo file vedranno le funzioni
// le funzioni di RLDATABASE_API come se fossero importate da una DLL, mentre questa DLL considera i simboli
// definiti con questa macro come esportati.
#ifdef RLDATABASE_EXPORTS
#define RLDATABASE_API __declspec(dllexport)
#else
#define RLDATABASE_API __declspec(dllimport)
#endif

// Questa classe viene esportata dalla DLL
class RLDATABASE_API CRLDatabase {
public:
	CRLDatabase(void);
	// TODO: aggiungere qui i metodi.
};

extern RLDATABASE_API int nRLDatabase;

RLDATABASE_API int fnRLDatabase(void);
