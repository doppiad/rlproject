// RLDatabase.cpp : Definisce le funzioni esportate per la DLL.
//

#include "pch.h"
#include "framework.h"
#include "RLDatabase.h"


// Esempio di variabile esportata
RLDATABASE_API int nRLDatabase=0;

// Esempio di funzione esportata.
RLDATABASE_API int fnRLDatabase(void)
{
    return 0;
}

// Costruttore di una classe esportata.
CRLDatabase::CRLDatabase()
{
    return;
}
