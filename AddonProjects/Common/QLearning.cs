﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using RLDataLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using OfficeOpenXml;

namespace Common
{
    public static class QLearning
    {
        private static readonly int States = (int)Math.Ceiling((double)((double)25000000 / (double)2500));//25000000;
        private static readonly int Actions = 9;
        private static readonly double[,] QTable = new double[States, Actions];

        private static double _lastDistanceFromGoal;
        private static double _distanceFromGoal;
        private static readonly ILog Log = CommonFeature.GetLogger();

        // Hyperparameters
        private static readonly double Alpha = 0.3;

        private static readonly double Gamma = 0.6;

        private static double _epsilon = 0.125;

        private static double _penalties = 0;
        private static double _reward = 0;
        private static int _actualState = 0;
        private static int _lastState = 0;
        private static int _lastAction = 0;
        private static readonly Random R = new Random();
        private static int _lastEpoch = 0;

        private static readonly ExcelPackage Excel = new ExcelPackage(new FileInfo("C:\\test\\qTable.xlsx"));

        private static double _oldValue;

        public static double ValueToSave;

        public static void SetLastEpoch(int epoch)
        {
            _epsilon = 3f / (float)epoch;
            _lastDistanceFromGoal = 0;
            _penalties = 0;
            _lastEpoch = epoch;
        }

        public static double GetEpsilon()
        {
            return _epsilon;
        }

        public static double GetLastReward()
        {
            return _reward;
        }

        public static void GetReward(int action, double speed)
        {
            _reward = (_lastDistanceFromGoal - _distanceFromGoal) * 25;
            if (_distanceFromGoal <= 11)
            {
                _reward += 10000;
            }
            if (action == 4 || action == 5 || action == 7 || action == 8)
            {
                _reward = _reward / 1.12f;
            }
            else if ((action == 0 || action == 2) && _reward > 0)
            {
                _reward = _reward / 1.3f;
            }
            if (speed > 1.4)
            {
                _penalties += 0.5;
                _reward -= (speed - 1.4) * (Math.Log10((3 * (speed - 1.4)) + 2) * 10);
            }
            else if (speed < 1.2)
            {
                _penalties += 0.5;
                _reward -= (1.4 - speed) * (Math.Log10((3 * (1.4 - speed)) + 2) * 10);
            }
            //Log.Error("Report variabili: speed: " + speed + " action: " + action + " penalties: " + _penalties + " distanza goal: " + _distanceFromGoal + " ultima distanza: " + _lastDistanceFromGoal + " Reward: " + _reward);
        }

        public static int GetAction(AgentValues ag)
        {
            int action = -1;
            List<int> zeros = new List<int>();
            double max = double.MinValue;
            _lastDistanceFromGoal = ag.dGoal;
            if (R.NextDouble() < _epsilon) //new actions
            {
                action = R.Next(0, 8);
            }
            else //already known actions
            {
                _actualState = GetCurrentState(ag.xPosition, ag.zPosition);
                for (int i = 0; i < 9; i++)
                {
                    var tmp = QTable[_actualState, i];

                    if (tmp > max)
                    {
                        max = QTable[_actualState, i];
                        action = i;
                    }
                    else if (tmp == 0.0d)
                    {
                        zeros.Add(i);
                        Log.Error("Zero aggiunto");
                    }
                }

                if (zeros.Any() && max == 0.0d)
                {
                    Log.Error("Zeri da leggere");
                    action = zeros[R.Next(zeros.Count)];
                    Log.Error("Zeri letti");
                }
            }
            _oldValue = QTable[_actualState, action];
            _lastAction = action;
            _lastState = _actualState;

            return action;
        }

        public static int Train(AgentValues ag)
        {
            if (ag.Epoch == 0 && ag.Episode == 0)
                return -1;
            _distanceFromGoal = ag.dGoal;
            int action = -1;
            double max = double.MinValue;
            _actualState = GetCurrentState(ag.xPosition, ag.zPosition);
            for (int i = 0; i < 9; i++)
            {
                if (QTable[_actualState, i] > max)
                {
                    max = QTable[_actualState, i];
                    action = i;
                }
            }
            GetReward(action, ag.speed);
            double newValue = (1 - Alpha) * _oldValue + Alpha * (_reward + Gamma * max) - _penalties;
            QTable[_lastState, _lastAction] = newValue;
            ValueToSave = newValue;
            ExportToExcel();

            return _actualState;
        }

        public static int GetCurrentState(int x, int z)
        {
            return x + Math.Abs(z * 100); //714 num elements x row
        }

        public static void ExportToExcel()
        {
            int s = _lastState;
            int a = _lastAction;
            double v = ValueToSave;
            ExcelWorksheet workSheet;
            // name of the sheet
            if (Excel.Workbook.Worksheets.Count == _lastEpoch)
            {
                workSheet = _lastEpoch > 0
                    ? Excel.Workbook.Worksheets.Add(_lastEpoch.ToString(), Excel.Workbook.Worksheets[_lastEpoch - 1])
                    : Excel.Workbook.Worksheets.Add(_lastEpoch.ToString());
                workSheet.DefaultRowHeight = 12;
                Excel.Save();
            }
            else
            {
                workSheet = Excel.Workbook.Worksheets[_lastEpoch];
            }

            workSheet.SetValue(s + 1, a + 1, v);
        }
    }
}