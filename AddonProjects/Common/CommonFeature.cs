﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;

namespace Common
{
    /// <summary>
    /// This class has been designed to hold all common methods along the projects
    /// this should avoid code repetition within the project
    /// The method are designed to avoid any code dependency
    /// </summary>
    public class CommonFeature
    {
        private static bool _defineLog = false;

        /// <summary>
        /// This is a simple test to configure Log4NET
        /// </summary>
        /// <returns>the logger to be used in some point of code</returns>
        public static ILog GetLogger()
        {
            if (!_defineLog)
            {
                _defineLog = true;
                XmlConfigurator.ConfigureAndWatch(new FileInfo("C:\\test\\log4net.Config"));
            }

            return LogManager.GetLogger("AdoNetAppender");
        }

        /// <summary>
        /// Return a random number between min and max value
        /// </summary>
        /// <param name="minimum">min</param>
        /// <param name="maximum">max</param>
        /// <returns></returns>
        public static double GetRandomNumber(double minimum, double maximum)
        {
            System.Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        /// <summary>
        /// Given a enumeration returns a random integer number value
        /// </summary>
        /// <param name="type">Enumeration typeof</param>
        /// <returns></returns>
        public static int GetRandomEnumerationValue(int max)
        {
            Random r = new Random();
            return r.Next(max);
        }
    }
}